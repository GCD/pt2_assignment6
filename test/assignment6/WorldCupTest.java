package assignment6;

import static org.junit.Assert.*;

import org.junit.Test;

public class WorldCupTest {

	@Test
	public void testExample() {
		int[] s = new int[] {1, 3,2,5};
		int[] e = new int[] {4,10,6,8};
		int[][] visits = new int[][] {{5},{2,6},{1,10,9}};
		int[] clips = WorldCup.numOfClips(s, e, visits);
		
		assertArrayEquals(new int[] {3,4,2}, clips);
	}
	
	@Test
	public void testExample2() {
		int[] s = new int[] {100,2,4,77,78};
		int[] e = new int[] {500,30,95,256,255};
		int[][] visits = new int[][] {{1},{10},{3},{77},{100},{256}};
		int[] clips = WorldCup.numOfClips(s, e, visits);
		
		assertArrayEquals(new int[] {0,2,1,2,3,2}, clips);
	}
}
