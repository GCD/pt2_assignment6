package assignment6;

import org.junit.Before;
import org.junit.Test;

import assignment6.hashing.MyVeryOwnHashFunction;

public class OwnHashTest {
	MyVeryOwnHashFunction hashFunction;
	
	@Before
	public void init() {
		this.hashFunction = new MyVeryOwnHashFunction();
	}
	@Test
	public void test() {
		System.out.println(hashFunction.hash("Nico"));
		System.out.println(hashFunction.hash("Sven"));
		System.out.println(hashFunction.hash("Julia"));
		System.out.println(hashFunction.hash("Nicola"));
		System.out.println(hashFunction.hash("Lulululululuulululululululululululul"));
		System.out.println(hashFunction.hash("s"));
		System.out.println(hashFunction.hash("a"));
		System.out.println(hashFunction.hash("b"));
		System.out.println(hashFunction.hash("c"));
		System.out.println(hashFunction.hash("d"));
		System.out.println(hashFunction.hash("e"));
		System.out.println(hashFunction.hash("f"));
	}
}
