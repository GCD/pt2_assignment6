package assignment6;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class ConnectedComponentFinderTest {
	WeightedGraph g;
	
	@Before
	public void graphSetup() {
		this.g = new WeightedGraph();
	}
	
	private void addNodes() {
		g.add("hallo", "welt", 1);
		g.add("a", "b", 1);
		g.add("a", "c", 1);
		g.add("a", "d", 2);
		g.add("x", "a", 1);
		
	}
	
	@Test
	public void testGraph() {
		addNodes();
		
		ConnectedComponentFinder c = new ConnectedComponentFinder();
		Set<Set<String>> comps  = c.connectedComponents(g);
		
		assertEquals(2, comps.size());
	}
	
	@Test
	public void testMain() {
		//ConnectedComponentFinder.main(new String[]{});
	}
}
