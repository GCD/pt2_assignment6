package assignment6;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class DijkstraTest {
	
	private Dijkstra pathFinder;
	private WeightedGraph graph;
	
	@Before
	public void init() {
		pathFinder = new Dijkstra();
	}
	
	@Test
	public void simpleTest() {
		graph = new WeightedGraph();
		
		graph.add("A", "B", 1);
		graph.add("A", "C", 2);
		graph.add("A", "D", 3);
		graph.add("B", "E", 4);
		graph.add("B", "F", 5);
		graph.add("C", "F", 6);
		graph.add("D", "F", 7);
		graph.add("D", "G", 8);
		graph.add("E", "H", 2);
		graph.add("F", "H", 2);
		graph.add("G", "H", 1);
		
		String[] path = pathFinder.shortestPath(graph, "A", "H");
		System.out.println(Arrays.toString(path));
		assertArrayEquals(new String[] {"A", "B", "E", "H"}, path);
		
		path = pathFinder.shortestPath(graph, "D", "H");
		System.out.println(Arrays.toString(path));
		assertArrayEquals(new String[] {"D", "F", "H"}, path);
	}
}
