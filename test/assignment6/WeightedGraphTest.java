package assignment6;

import static org.junit.Assert.*;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class WeightedGraphTest {
	WeightedGraph g;
	
	@Before
	public void graphSetup() {
		this.g = new WeightedGraph();
	}
	
	private void addNodes() {
		g.add("hallo", "welt", 1);
		g.add("a", "b", 1);
		g.add("a", "c", 1);
		g.add("a", "d", 2);
	}
	
	@Test
	public void testAdd() {
		addNodes();
		
		assertEquals(6, g.verticeCount());
		assertEquals(4, g.edgeCount());
	}
	
	@Test
	public void testAdd2() {
		g.add("a", "b", 0.9299349293239d);
		g.add("a", "c", 0.1343456345239d);
		g.add("a", "d", 0.0249249235522d);
		g.add("b", "a", 0.2493208450925d);
		g.add("b", "c", 0.7529448492409d);
		g.add("c", "d", 0.8923423892123d);
		
		g.add("Carter, Jim (I)", "Van Damme, Jean-Claude", 0.75);
	}
	
	@Test
	public void testAdjacent() {
		addNodes();
		
		Iterator<String> it = g.adjacentOf("hallo").iterator();
		assertEquals("welt", it.next());
		
		it = g.adjacentOf("a").iterator();
		assertEquals("b", it.next());
		assertEquals("c", it.next());
		assertEquals("d", it.next());
		
		it = g.adjacentOf("b").iterator();
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testWeight() {
		addNodes();
		
		assertEquals(2, g.weight("a", "d"), 0.1d);
		assertEquals(1, g.weight("a", "c"), 0.1d);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testWeightException() {
		addNodes();
		
		g.weight("hallo", "c");
	}
}
