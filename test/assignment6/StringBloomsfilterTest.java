package assignment6;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import assignment6.hashing.JavaMDHashFunction;

public class StringBloomsfilterTest {

	private StringBloomsfilter bfilter;
	
	@Before
	public void init() {
		this.bfilter = new StringBloomsfilter(10000, JavaMDHashFunction.MD5, JavaMDHashFunction.SHA1);
		
		bfilter.add("Hallo Welt!");
		bfilter.add("Programmiertechnik2");
		bfilter.add("HashFunction");
		bfilter.add("Bloomsfilter");
		bfilter.add("Nico Ring");
		bfilter.add("Sven Mischkewitz");
		bfilter.add("Hallo Welt!");
	}
	
	@Test
	public void elementCountTest() {
		assertEquals(7, bfilter.elementCount());
	}
	
	@Test
	public void positiveTest() {
		assertEquals(true, bfilter.mightContain("Nico Ring"));
		assertEquals(true, bfilter.mightContain("Hallo Welt!"));
		assertEquals(true, bfilter.mightContain("Programmiertechnik2"));
		assertEquals(true, bfilter.mightContain("Bloomsfilter"));
	}
	
	@Test
	public void negativeTest() {
		assertEquals(false, bfilter.mightContain("Not in Bloomsfilter"));
	}
}
