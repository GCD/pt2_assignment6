package assignment6.interval;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

public class IntervalTree<T extends Interval> {
	private final IntervalTree<T> left;
	private final IntervalTree<T> right;
	private NavigableSet<T> intervals;
	
	public IntervalTree(List<T> intervals) {
		final List<T> leftIntervals = new ArrayList<>();
		final List<T> rightIntervals = new ArrayList<>();
		
		// calculate rang midpoint
		int center = getCenter(intervals);
		
		// build set
		this.intervals = new TreeSet<>();
		// divide into left, right and center lists
		for (T i : intervals) {
			if (i.intersects(center)) {
				this.intervals.add(i);
			} else if (i.end() < center) {
				leftIntervals.add(i);
			} else if (i.start() > center) {
				rightIntervals.add(i);
			}
		}
		
		// assign subtress
		left = leftIntervals.size() > 0 ? new IntervalTree<T>(leftIntervals) : null;
		right = rightIntervals.size() > 0 ? new IntervalTree<T>(rightIntervals) : null;
	}
	
	private int getCenter(List<T> intervals) {
		int minStart = Integer.MAX_VALUE;
		int maxEnd = Integer.MIN_VALUE;
		
		for (T i : intervals) {
			if (i.start() < minStart) {
				minStart = i.start();
			}
			
			if (i.end() > maxEnd) {
				maxEnd = i.end();
			}
		}
		
		return (maxEnd + minStart) / 2;
	}
	
	public List<T> getIntersectingIntervals(int value) {
		return travel(value, new LinkedList<T>());
	}
	
	public List<T> travel(int value, List<T> intersecting) {
		for (T i : intervals) {
			if (i.start() > value) {
				break;
			}
			
			if (i.intersects(value)) {
				intersecting.add(i);
			}
		}
		
		if (left != null) {
			left.travel(value, intersecting);
		}
		
		if (right != null) {
			right.travel(value, intersecting);
		}
		
		return intersecting;
	}
}
