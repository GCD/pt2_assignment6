package assignment6.interval;

public class Interval implements Comparable<Interval> {
	private final int start;
	private final int end;
	
	public Interval(int start, int end) {
		this.start = start;
		this.end = end;
	}
	
	public int start() {
		return start;
	}
	
	public int end() {
		return end;
	}
	
	public boolean intersects(int value) {
		return start <= value && value <= end;
	}

	@Override
	public int compareTo(Interval i) {
		return start - i.start();
	}
	
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		
		if (other instanceof Interval) {
			return start == ((Interval) other).start() && end == ((Interval) other).end();
		}
		
		// fallback
		return false;
	}
}
