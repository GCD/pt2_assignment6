
package assignment6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import assignment6.interval.*;

public class WorldCup {
	
	public static int[] numOfClips(int[] s, int[] e, int[][] visits) {
		
		// store values as interval list
		List<Interval> intervals = new ArrayList<>();
		for (int i = 0; i < s.length; i++) {
			intervals.add(new Interval(s[i], e[i]));
		}
		
		// build tree
		IntervalTree<Interval> tree = new IntervalTree<>(intervals);
		
		// init return array
		int[] clips = new int[visits.length];
		
		// loop crews
		for (int c = 0; c < visits.length; c++) {
			Set<Interval> clipHash = new HashSet<>();
			
			// loop visits and store possible clips
			for (int t = 0; t < visits[c].length; t++) {
				// retrieve all available clips by testing intersecting time intervals
				clipHash.addAll( tree.getIntersectingIntervals(visits[c][t]) );
			}
			
			// store clip count
			clips[c] = clipHash.size();
		}
		
		return clips;
	}
}
