package assignment6;

import assignment6.hashing.HashFunction;

/**
 * class for storing String in a blooms filter
 * @author toni.gruetze
 *
 */
public class StringBloomsfilter {
	private final boolean[] bits;
	private final HashFunction<String>[] hashFunctions;
	private int elementCount = 0;
	
	/**
	 * creates a new BloomsFilter for strings, based on the specified hash functions
	 * @param m size of the bit array
	 * @param hashFunctions the hash functions
	 */
	@SafeVarargs
	public StringBloomsfilter(int m, HashFunction<String>... hashFunctions) {
		if(m<1 || hashFunctions==null || hashFunctions.length<=0) {
			throw new IllegalArgumentException();
		}
		this.hashFunctions = hashFunctions;
		bits = new boolean[m];
	}
	
	/**
	 * adds v to the filter
	 * @param v the element to add
	 */
	public void add(String v) {
		for (HashFunction<String> hashFunction : hashFunctions) {
			bits[Math.abs(hashFunction.hash(v)) % bits.length] = true;
		}
		elementCount++;
	}
	
	/**
	 * checks if w might be element of this collection
	 * @param w the element to be checked
	 */
	public boolean mightContain(String w) {
		for (HashFunction<String> hashFunction : hashFunctions) {
			if (bits[Math.abs(hashFunction.hash(w)) % bits.length] != true){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * gets the number of strings added to the filter
	 * @return the number of string elements n
	 */
	public int elementCount() {
		return elementCount;
	}
	
	/**
	 * gets the estimated false positive probability of this filter
	 * @return the probability for false positives
	 */
	public double probFP() {
		int k = hashFunctions.length;
		int n = elementCount();
		int m = bits.length;

		return  Math.pow(1 - Math.pow((1 - 1 / m), k * n) , k);
	}
}
