package assignment6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WeightedGraph {
	private class Edge {
		public String node;
		public double weight;
		
		public Edge(String n, Double w) {
			node = n;
			weight = w;
		}
	}

	// increase hashtable size because cast-file is too large
	private HashMap<String, List<Edge>> map = new HashMap<String, List<Edge>>();
	private Set<String> nodes = new HashSet<String>();
	
	public int verticeCount() {
		return nodes.size();
	}
	
	public int edgeCount() {
		
		int edges = 0;
		String vertex;
		
		Iterator<String> it = nodes.iterator(); 
		
		while (it.hasNext()) {
			vertex = it.next();
			if (map.containsKey(vertex)) {
				edges += map.get(vertex).size();
			}
		}
		
		return edges;
	}
	
	public double weight(String from, String to) {
		if (!map.containsKey(from)) {
			throw new IllegalStateException("edge does not exist: "+ from +" -> "+to);
		}
		
		for (Edge current : map.get(from)) {
			if (current.node == to) {
				return current.weight;
			}
		}

		// fallback
		throw new IllegalStateException("edge does not exist: "+ from +" -> "+to);
	}

	public void add(String from, String to, double weight) {
		if (!map.containsKey(from)) {
			map.put(from, new LinkedList<Edge>());
		}
		
		List<Edge> edges = map.get(from);
		Iterator<Edge> it = edges.iterator();
		
		Edge current;
		
		while (it.hasNext()) {
			current = it.next();
			if (current.node == to) {
				it.remove();
			}
		}
		
		nodes.add(to);
		nodes.add(from);
		edges.add(new Edge(to, weight));
	}
	
	public Iterable<String> vertices() {
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				return nodes.iterator();
			}
		};
	}
	
	public Iterable<String> adjacentOf(String vertex) {
		final String node = vertex;
		return new Iterable<String>() {
			
			@Override
			public Iterator<String> iterator() {
				final List<String> nodes = new ArrayList<String>();

				if (map.containsKey(node)) {
					for (Edge current : map.get(node)) {
						nodes.add(current.node);
					}
				}
				
				return nodes.iterator();
			}
		};
	}
	
}
