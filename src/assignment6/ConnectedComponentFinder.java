package assignment6;

import java.util.HashSet;
import java.util.Set;

public class ConnectedComponentFinder {
	public Set<Set<String>> connectedComponents2(final WeightedGraph g) {
		Set<Set<String>> components = new HashSet<Set<String>>();
		Set<String> treenodes = new HashSet<String>();
		
		makeBidirectional(g);
		for (String vertex : g.vertices()) {
			
			// skip if exists
			if (treenodes.contains(vertex)) {
				continue;
			}
			
			Set<String> subtree = new HashSet<String>();
			subtree.add(vertex);
			
			travel(vertex, g, subtree, treenodes);
			
			components.add(subtree);
			
		}

		return components;
	}
	
	private Set<Set<String>> components = new HashSet<Set<String>>();
	
	public Set<Set<String>> connectedComponents(WeightedGraph g) {

		for (String node : g.vertices()) {
			Set<String> setOfNode = getSet(node);
			
			if (setOfNode == null) {
				Set<String> tmp = new HashSet<String>();
				tmp.add(node);
				components.add(tmp);
				setOfNode = tmp;
			}
			
			Iterable<String> neighbours = g.adjacentOf(node);
			
			for (String neighbour : neighbours) {
				Set<String> setOfNeighbour = getSet(neighbour);
				
				if (setOfNeighbour == null) {
					setOfNode.add(neighbour);
				} else if (setOfNeighbour != setOfNode) {
					// add the minor set to the bigger set and delete the minor set in components
					if (setOfNeighbour.size() > setOfNode.size()) {
						setOfNeighbour.addAll(setOfNode);
						components.remove(setOfNode);
					} else {
						
						setOfNode.addAll(setOfNeighbour);
						components.remove(setOfNeighbour);
					}
				}
			}
		}
		
		return components;
	}
	
	private Set<String> getSet(String node) {
		for (Set<String> set : components) {
			if (set.contains(node)) {
				return set;
			}
		}
		return null;
	}
	
	
	private void travel(String vertex, WeightedGraph graph, Set<String> subtree, Set<String> treenodes) {
		
		for (String node : graph.adjacentOf(vertex)) {
			
			// skip if exists
			if (treenodes.contains(node) || subtree.contains(node)) {
				continue;
			}
			
			// remember this node
			subtree.add(node);
			treenodes.add(node);
			
			// lookup next
			travel(node, graph, subtree, treenodes);
			
		}

	}
	
	private void makeBidirectional(WeightedGraph g) {
		for (String vertex : g.vertices()) {
			for (String node : g.adjacentOf(vertex)) {
				g.add(node, vertex, g.weight(vertex, node));
			}
		}
	}

	public static void main(String[] args) {
		GraphFileReader reader = new GraphFileReader();
		
		WeightedGraph g = reader.read("./", "cast.action.txt");
		
		ConnectedComponentFinder finder = new ConnectedComponentFinder();
		Set<Set<String>> comonentSet = finder.connectedComponents(g);
		System.out.println(comonentSet.size() + " connected components found:");
		
		for(Set<String> c : comonentSet) {
			System.out.print(c.size());
			System.out.print(":\t");
			int i=0;
			for(String el : c) {
				System.out.print(el);
				++i; 
				if(i>5)
					break;
				if(i<c.size()) {
					System.out.print(" & ");
				}
			}
			if(c.size()>5)
				System.out.print(" & ...");
			System.out.println();
		}
	}
}
