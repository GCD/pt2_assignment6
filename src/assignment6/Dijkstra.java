package assignment6;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Stack;

public class Dijkstra {
	
	private HashMap<String, Double> distances;
	private PriorityQueue<String> minNodes;
	private HashMap<String, String> nextHop;
	
	public String[] shortestPath(WeightedGraph g, String startNode, String endNode) {
		
		distances = new HashMap<String, Double>(g.verticeCount());
		nextHop = new HashMap<>(g.verticeCount());
		minNodes = new PriorityQueue<>(g.verticeCount(), new VertexComparator());
		
		Iterator<String> nodes = g.vertices().iterator();
		while (nodes.hasNext()) {
			String next = nodes.next();
			
			if (next == startNode) {
				continue;
			}
			
			distances.put(next, Double.POSITIVE_INFINITY);
			minNodes.add(next);
		}
		
		distances.put(startNode, 0d);
		minNodes.add(startNode);
		
		while(!minNodes.isEmpty()) {
			String currentNode = minNodes.poll();
			
			Iterable<String> nextNodes = g.adjacentOf(currentNode);
			
			for (String nextNode : nextNodes) {
				if (!minNodes.contains(nextNode)) {
					continue;
				}
				
				Double distanceOverNextNode = distances.get(currentNode) + g.weight(currentNode, nextNode);
				if (distanceOverNextNode < distances.get(nextNode)) {
					nextHop.put(nextNode, currentNode);
					distances.put(nextNode, distanceOverNextNode);
					minNodes.remove(nextNode);
					minNodes.add(nextNode);
				}
			}
			
		}
		
		Stack<String> shortestPath = new Stack<String>();
		String node = endNode;
		
		do {
			shortestPath.add(node);
			node = nextHop.get(node);
		} while (node != startNode);
		
		shortestPath.add(startNode);
		
		String[] arr = new String[shortestPath.size()];
		Collections.reverse(shortestPath);
		shortestPath.toArray(arr);
		
		
		return arr;
	}
	
	private class VertexComparator implements Comparator<String>{

		@Override
		public int compare(String vertex1, String vertex2) {
			
			return (int) (distances.get(vertex1) - distances.get(vertex2));
		}
	}
}
