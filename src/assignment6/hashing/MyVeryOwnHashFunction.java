package assignment6.hashing;

import java.io.UnsupportedEncodingException;

public class MyVeryOwnHashFunction implements HashFunction<String>{

	@Override
	public int hash(String input) {
		int hash = 23;
		byte[] bytes = null;
		try {
			bytes = input.getBytes("UTF-8");
			
			for (int i = 0; i < bytes.length; i++) {
//				System.out.println(bytes[i]);
//				hash = (hash >> (int) (i * Math.log10(hash))) ^ 31 & bytes[i];
				hash = (hash * 33) ^ bytes[i];

			}
				
				
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return Math.abs(hash);
	}

}
