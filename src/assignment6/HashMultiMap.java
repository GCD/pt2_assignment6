package assignment6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import assignment6.hashing.DefaultHashFunction;
import assignment6.hashing.HashFunction;


/**
 * An associative array that is able to bind multiple values (elements) to one single key.
 * The implementation is based on a specific hash function
 * @param <K> the type of keys to be stored
 * @param <V> the type of elements to be stored
 */
public class HashMultiMap<K, V> {
	private final HashFunction<K> hashFunction;
	
	private ArrayList<LinkedList<V>> hashTable;
	
	/**
	 * default constructor
	 */
	public HashMultiMap() {
		this(1024, new DefaultHashFunction<K>());
	}
	
	/**
	 * 
	 * @param tableSize size of the hashtable
	 * @param hashFunction the hashing function to be used
	 */
	public HashMultiMap(int tableSize, HashFunction<K> hashFunction) {
		
		//initialize hash function
		this.hashFunction = hashFunction;
		
		// initialize hash table
		this.hashTable = new ArrayList<LinkedList<V>>(tableSize);
		
		for (int i = 0; i < tableSize; i++) {
			hashTable.add(i, new LinkedList<V>());
		}
	}
	
	/**
	 * adds a new "binding" (key-value pair) to the multi-map
	 * @param key the key to be added
	 * @param element the element to be added
	 */
	public void bind(K key, V element) {
		
		//get hash value modulo tableSize
		int hashKey =  hashFunction.hash(key) % hashTable.size();
		
		// add element in the list of the given key
		hashTable.get(Math.abs(hashKey)).add(element);
	}
	
	/**
	 * Removes all bindings (key-value pairs) with the specified key from the multi-map
	 * @param key the key to be removed  (hash code identical and <code>key.equals(y)</code>)
	 */
	public void removeBindingFor(K key) {
		
		//get hash value modulo tableSize
		int hashKey =  hashFunction.hash(key) % hashTable.size();
		
		//delete all items in the list of this key
		hashTable.get(Math.abs(hashKey)).clear();
	}
	
	/**
	 * gets the number of bindings for a specific key is present in this multi-map
	 * @param key the key to be checked (hash code identical and <code>key.equals(y)</code>)
	 * @return <code>0</code> if one or more bindings exist for this instance, otherwise the count of associated values
	 */
	public int countElementsFor(K key) {
		
		//get hash value modulo tableSize
		int hashKey =  hashFunction.hash(key) % hashTable.size();
		
		// return length of the list stored in the field
		return hashTable.get(Math.abs(hashKey)).size();
	}
	
	/**
	 * Gets all values associated with the key
	 * @param key the key (hash code identical and <code>key.equals(y)</code>)
	 * @return An iterator over all values associated with this key
	 */
	public Iterator<V> elementsFor(K key) {
		
		//get hash value modulo tableSize
		int hashKey =  hashFunction.hash(key) % hashTable.size();
		
		// return iterator of the list stored in the field
		return hashTable.get(Math.abs(hashKey)).iterator();
	}
	
	/**
	 * gets the number of bindings (key-value pairs) in the multi-map.
	 * @return the number of bindings
	 */
	public int count() {
		
		int count = 0;
		for (LinkedList<V> keyList : hashTable) {
			count += keyList.size();
		}
		return count;
	}
}
